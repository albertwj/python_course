#Recursive
# 1. call itself
def recursive(n):
    #2. exit condition
    if n == 0:
        return

    #3. n immutable: n different at each level
    print(n)
    recursive(n-1) # call itself
    print(n)
    return

recursive(10)

#list : mutable => in_list equals to my_list
def new_recursive(in_list):
    in_list.append("in_function")

my_list = []
my_list.append("outside")
new_recursive(my_list)
print(my_list)

#immutable: n not equals to my_data
def immu_recursive(n):
    n+=1

my_data = 3
immu_recursive(my_data)
print(my_data)


#list
my_list = []
my_list.append(3)
my_list.append(4)

my_list[1]

#Maze, why works-> immutable local variable, different at each level
def go_maze(board, x, y):
    #1. exit condition
    if x == 2 and y == 2:
        print(str(x) + " , " + str(y))#data type conversion
        return True
    
    #when to return
    #1. out of index, 2. order matters
    if y >= 3 or x >= 3 or board[x][y] == 1: # current on a wall; board[0][1] = 1
        return False

    #try different decisions
    #test right direction
    if go_maze(board, x, y + 1) == False:
        #test down direction
        if go_maze(board, x + 1, y) == False:
            return False
        
    #ok we find solution
    print(str(x) + " , " + str(y))#data type conversion
    return True

#0 1 0
#0 1 0
#0 0 0
board = []
board.append([])
board.append([])
board.append([])
board[0].append(0)
board[0].append(1)
board[0].append(0)
board[1].append(0)
board[1].append(1)
board[1].append(0)
board[2].append(0)
board[2].append(0)
board[2].append(0)
print(board)

if go_maze(board, 0, 0) == True:
    print("Good")
