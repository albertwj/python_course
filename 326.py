import numpy as np
import matplotlib.pyplot as plt

#1 data process
data = np.genfromtxt("data.txt", delimiter=',')
#relative location vs absolute location
avg_2nd = sum(data[1::2,2])/len(data[1::2,2])
#data[1::2,2] ~ a:b:c ~ start from a until b, step c
std_2nd = np.std(data[:,2])

for i in range(len(data)):#for each row
    if sum(data[i,1::])/len(data[i,1::]) > 500:
        plt.plot(data[i,:], label="Test"+str(i))

plt.xlabel('Day')
plt.ylabel('Price')
plt.title('Price for NP')
plt.legend()
plt.show()
